export default class FileEntry {
    /**
     * Initializes a new instance of the FileEntry class with the specified name and content.
     *
     * @param {string} sectionName - The section name.
     * @param {string} fileName - The file name.
     */
    constructor(sectionName, fileName) {
        this.sectionName = sectionName;
        this.fileName = fileName;
    }
}