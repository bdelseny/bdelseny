export default class Loader {
  /**
   * Initializes a new instance of the Loader class with the specified URL.
   * If no URL is provided, the default URL is set to the current document's URL appended with "/static".
   *
   * @param {string} [url=window.location.href + "/static"] - The URL to be used for loading files.
   */
  constructor(lang = "fr", url = `${window.location.href}static`) {
    this.lang = lang;
    this.url = url;
  }

  /**
   * Asynchronously loads a file from the specified URL and returns its contents as a string.
   *
   * @param {string} file - The name of the file to load.
   * @return {Promise<string>} A promise that resolves to the contents of the file as a string.
   */
  async loadFile(file) {
    const response = await fetch(`${this.url}/${this.lang}/${file}`);
    const text = await response.text();
    return text;
  }
}
