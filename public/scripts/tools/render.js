/** @see https://github.com/markdown-it/markdown-it */
import MarkDownIt from "https://cdn.jsdelivr.net/npm/markdown-it/+esm";

export default class MarkIt {
  constructor() {
    this.markdown = new MarkDownIt();
  }

  /**
   * Renders the given text using the markdown renderer.
   *
   * @param {string} text - The text to be rendered.
   * @return {string} The rendered HTML.
   */
  render(text) {
    return this.markdown.render(text);
  }
}
