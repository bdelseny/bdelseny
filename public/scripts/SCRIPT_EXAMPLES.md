Snippets to use for CV

Below is a simple example of how to use the IntersectionObserver API.

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Intersection Observer Example</title>
    <style>
        body {
            height: 200vh; /* Make the body tall to enable scrolling */
            display: flex;
            flex-direction: column;
            align-items: center;
        }
        .box {
            width: 100px;
            height: 100px;
            margin: 50px;
            background-color: lightblue;
            transition: background-color 0.3s;
        }
        .active {
            background-color: coral; /* Change color when active */
        }
    </style>
</head>
<body>
    <div class="box"></div>
    <div class="box"></div>
    <div class="box"></div>
    <div class="box"></div>
    <div class="box"></div>

    <script>
        // Callback function for the IntersectionObserver
        const callback = (entries) => {
            entries.forEach(entry => {
                if (entry.isIntersecting) {
                    entry.target.classList.add('active');
                } else {
                    entry.target.classList.remove('active');
                }
            });
        };

        // Create an IntersectionObserver instance
        const observer = new IntersectionObserver(callback);

        // Select all elements to observe
        const boxes = document.querySelectorAll('.box');
        boxes.forEach(box => {
            observer.observe(box); // Start observing each box
        });
    </script>
</body>
</html>
```