/**
 * Organize all that should be done in the main script.
 *
 * @author Bastien Delseny
 * @module
 */
import MarkIt from "./tools/render.js";
import Loader from "./tools/loader.js";
import FileEntry from "./models/fileEntry.js";

const lang = {
  en: "en",
  fr: "fr"
};

/** Files to load for the Webpage */
const FILES = Object.freeze([
  new FileEntry("Home", "intro.md"),
  new FileEntry("Experiences", "experiences.md"),
  new FileEntry("Formations", "formations.md"),
  new FileEntry("Interests", "interests.md"),
  new FileEntry("Projects", "projects.md"),
  new FileEntry("Skills", "skills.md"),
  new FileEntry("Contact", "contact.md"),
]);

const loader = new Loader(lang[document.documentElement.lang] || "fr");

const promises = [];

for (const file of FILES) {
  promises.push(loader.loadFile(file.fileName));
}

Promise.all(promises)
  .then((results) => {
    for (let i = 0; i < results.length; i++) {
      const text = results[i];
      
      document.body.innerHTML += /*html*/`
        <section id="${FILES[i].sectionName}">
          <h1>${FILES[i].sectionName}</h1>
          ${(new MarkIt()).render(text)}
        </section>
      `;
    }
  })
  .catch((error) => {
    console.error(error);
  });
