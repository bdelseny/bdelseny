Python et JavaScript sont mes langages de prédilection.
Comprendre le fond des choses et leur fonctionnement sont pour moi essentiel,
je privilégie donc le développement avec le minimum de framework possible et les plus restreints dans leurs tâches.

Python est probablement mon langage préféré, car libre et dont la librairie standard font pâlir plus d'un langage.
Simple à manier, sans superflu, mais permettant de descendre sur les couches basses d'un système et en tant que serveur Web par sa robustesse,
il permet également par sa souplesse à toucher d'autres corps de métiers autours notamment des statistiques.
Ainsi il allie mes deux centres d'intérêts informatiques fonctionnels : les statistiques et le Web.

Le combo habituel du Web: HTML, CSS et JavaScript me permet d'un autre côté de laisser parler ma créativité.
JavaScript et sa souplesse en font un allier plus que précieux pour rendre les pages interactives mais surtout attirantes.
Beaucoup partent sur son cousin typé TypeScript mais pour ma part je préfère la souplesse que laisse le JavaScript et que je trouve plus adapté au front.

Enfin, bien que ces langages soient mes favoris, mon expérience professionnelle m'a amener à côtoyer d'autres langages comme le Java, le PHP, le Bash, etc.

*WIP*
