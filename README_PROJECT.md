Where my CV is hosted. It will be my personal, customized page.

## Techno

I like being as close to the raw stuff, Node and NPM does not help doing so, as
well as big frameworks such as Angular, React or Vue.

Hence, I will be using Deno, to have a small handler for JavaScript and for CLI.
It will help using Web standards, more than Node - which can be great for other
stuff.

## Vision

I want it to be simple to add content, and separate the content from the
rendering. I could use HTML, as it is meant for structure and content, but it
can be verbose to write, so I will go with MarkDown. I find MarkDown can be both
simple to write and also powerful enough, and when it's not enough let's just go
back to HTML.

Here is how I see it:

- Sections will be written in MarkDown
- 1 file per section - maybe 1 folder, I am still not sure
- JavaScript to load and render MarkDown to HTML
- CSS to make it beautiful - or catching in my eyes
- JavaScript to make the page a little more interactive
